#AWS vars
variable "aws_access_key" {
  description = "AWS access key, injected by Terraform Cloud"
}

variable "aws_secret_key" {
  description = "AWS secret key, injected by Terraform Cloud"
}

variable "aws_region" {
  type    = string
  default = "eu-central-1"
}
