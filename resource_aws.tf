#Create IAM role and policy
resource "aws_iam_role" "role" {
  name               = "StartStopRole"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "policy" {
  name   = "StartStopInstances"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeInstances",
                "ec2:StartInstances",
                "logs:*",
                "ec2:StopInstances"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "policy_role" {
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.policy.arn
}

resource "aws_iam_instance_profile" "instance_role" {
  name = "startStop"
  role = aws_iam_role.role.name
}

resource "aws_lambda_function" "lambda" {
  filename      = "lambda_code.zip"
  function_name = "AutoStartStop"
  role          = aws_iam_role.role.arn
  handler       = "lambda_code.lambda_handler"
  timeout       = 30
  runtime = "python3.8"
}

resource "aws_cloudwatch_event_rule" "start" {
  name                = "StartInstances"
  schedule_expression = "cron(0 7 ? * * *)" #Start every morning at 7 AM GMT, change according to your needs.
  is_enabled          = false               #By default we don't auto start the instances. Enable the rule to do so.
}

resource "aws_cloudwatch_event_rule" "stop" {
  name                = "StopInstances"
  schedule_expression = "cron(0 23 ? * * *)" #Stop every night at 11 PM GMT
}

resource "aws_cloudwatch_event_target" "start" {
  rule      = aws_cloudwatch_event_rule.start.name
  arn       = aws_lambda_function.lambda.arn
  input     = "{\"action\":\"start\"}"
}

resource "aws_cloudwatch_event_target" "stop" {
  rule      = aws_cloudwatch_event_rule.stop.name
  arn       = aws_lambda_function.lambda.arn
  input     = "{\"action\":\"stop\"}"
}