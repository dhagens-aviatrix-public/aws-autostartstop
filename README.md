# AWS-AutoStartStop

This deploys a Lambda script, IAM role and policy and Cloudwatch rules for automatically starting and stopping EC2 instances that are tagged with "Auto-StartStop-Enable" at times set in the Cloudwatch rules.

This is based on this source with some minor modifications: https://www.youtube.com/watch?v=urifcfKeuqI


The following variables are required:

key | value
--- | ---
aws_access_key | API Key for AWS
aws_secret_key | Secret belonging to above key

The following variables are optional:

key | default | value
--- | --- | ---
aws_region | eu-central-1 | Region to deploy Lambda in